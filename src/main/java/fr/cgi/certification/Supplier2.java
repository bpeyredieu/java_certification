package fr.cgi.certification;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Supplier2 {

    /**
     * Voici une série d'exercices pour travailler avec les `Supplier` en Java, adaptés à Java 17. Ces exercices vous permettront de comprendre et de maîtriser les différentes utilisations des `Supplier` dans vos programmes Java.
     *
     * ### Exercice 1: Création et Utilisation de Base
     * **Objectif**: Comprendre comment créer et utiliser un `Supplier` simple pour retourner un résultat.
     *
     * **Énoncé**:
     * - Créez un `Supplier` qui génère et retourne un nombre aléatoire.
     * - Utilisez ce `Supplier` pour imprimer un nombre aléatoire.
     *
     * ### Exercice 2: Fournisseur de Liste
     * **Objectif**: Utiliser un `Supplier` pour fournir une collection.
     *
     * **Énoncé**:
     * - Définissez un `Supplier` qui retourne une nouvelle liste de chaînes de caractères.
     * - Ajoutez quelques éléments à cette liste dans votre méthode principale, puis imprimez la liste.
     *
     * ### Exercice 3: `Supplier` avec des Expressions Lambda
     * **Objectif**: Pratiquer l'utilisation des expressions lambda avec les `Supplier`.
     *
     * **Énoncé**:
     * - Écrivez un `Supplier` qui utilise une expression lambda pour retourner la date et l'heure actuelle.
     * - Imprimez le résultat obtenu à partir de ce `Supplier`.
     *
     * ### Exercice 4: Combinaison de `Supplier` et `Consumer`
     * **Objectif**: Apprendre à combiner les `Supplier` avec d'autres interfaces fonctionnelles comme `Consumer`.
     *
     * **Énoncé**:
     * - Créez un `Supplier` qui génère un nouveau objet `Person` (avec un nom et un âge aléatoires).
     * - Utilisez un `Consumer` pour imprimer les détails de l'objet `Person` fourni par le `Supplier`.
     *
     * ### Exercice 5: Utilisation de `Supplier` dans une Méthode
     * **Objectif**: Intégrer les `Supplier` dans des méthodes pour fournir des données de manière flexible.
     *
     * **Énoncé**:
     * - Écrivez une méthode qui prend un `Supplier` comme paramètre et utilise le résultat fourni par le `Supplier` pour effectuer une opération (par exemple, calculer le carré d'un nombre).
     * - Testez cette méthode avec différents types de `Supplier`.
     *
     * Ces exercices vous aideront à maîtriser le concept de `Supplier` en Java, en explorant différentes façons de les créer et de les utiliser dans des scénarios pratiques.
     */

    public static void main1(String[] args) {
        Supplier<Integer> randomSupplier = () -> new Random().nextInt();
        // Utilisation du Supplier pour obtenir un nombre aléatoire
        System.out.println(randomSupplier.get());
    }

    public static void main2(String[] args) {
        Supplier<List<String>> listSupplier = ArrayList::new;
        List<String> myList = listSupplier.get();
        myList.add("Java");
        myList.add("Supplier");
        myList.add("Exercise");

        // Imprimer la liste
        System.out.println(myList);
    }
    public static void main(String[] args) {
        Supplier<LocalDateTime> dateTimeSupplier = LocalDateTime::now;
        // Utilisation du Supplier pour obtenir la date et l'heure actuelle
        System.out.println(dateTimeSupplier.get());
    }

    public static void main4(String[] args) {
        Supplier<Person> personSupplier = () -> {
            Random random = new Random();
            int age = random.nextInt(100); // Génère un âge aléatoire entre 0 et 99
            return new Person("Person" + age, age);
        };

        Consumer<Person> personConsumer = person -> System.out.println("Name: " + person.getName() + ", Age: " + person.getAge());

        // Créer une personne et consommer l'information
        personConsumer.accept(personSupplier.get());
    }















    public static void main5(String[] args) {
        Supplier<Double> numberSupplier = () -> 9.0;
        System.out.println("Le carré est: " + calculateSquare(numberSupplier));
    }

    public static double calculateSquare(Supplier<Double> numberSupplier) {
        return Math.pow(numberSupplier.get(), 2);
    }


}
