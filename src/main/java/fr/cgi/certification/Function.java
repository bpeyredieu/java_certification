package fr.cgi.certification;

public class Function {

    /**
     *
     * Exercice 1: Transformation de Chaînes
     * Objectif: Utiliser une Function pour convertir des chaînes de caractères en leur longueur.
     *
     * Énoncé:
     *
     * Créez une Function<String, Integer> qui prend une chaîne de caractères et retourne sa longueur.
     * Testez cette fonction avec plusieurs chaînes.
     * Exercice 2: Calcul Mathématique
     * Objectif: Utiliser une Function pour effectuer un calcul simple.
     *
     * Énoncé:
     *
     * Définissez une Function<Integer, Integer> qui calcule le carré d'un nombre.
     * Appliquez cette fonction à une liste de nombres et affichez les résultats.
     * Exercice 3: Manipulation de Listes
     * Objectif: Transformer des éléments d'une liste en utilisant une Function.
     *
     * Énoncé:
     *
     * Utilisez une Function<List<Integer>, List<Integer>> pour doubler chaque élément d'une liste.
     * Testez cette fonction avec une liste d'entiers.
     * Exercice 4: Composition de Functions
     * Objectif: Combiner plusieurs Function pour créer des transformations en chaîne.
     *
     * Énoncé:
     *
     * Créez une Function<String, String> pour supprimer les espaces d'une chaîne de caractères et une autre pour convertir en majuscules.
     * Composez ces fonctions et appliquez-les à une chaîne de caractères.
     * Exercice 5: Conversion de Types
     * Objectif: Utiliser une Function pour convertir d'un type à un autre.
     *
     * Énoncé:
     *
     * Écrivez une Function<String, Boolean> qui convertit une chaîne de caractères en un booléen (par exemple, "true" en true, autre chose en false).
     * Testez cette fonction avec différentes chaînes.
     */

}
