package fr.cgi.certification;

public interface StringStart {

    boolean beginningCheck(String prefix);
}
