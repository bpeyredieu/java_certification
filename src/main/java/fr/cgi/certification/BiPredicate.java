package fr.cgi.certification;

public class BiPredicate {


    /**
     * Exercice 1: Comparaison Simple
     * Objectif: Utiliser un BiPredicate pour comparer deux chaînes de caractères par leur longueur.
     *
     * Énoncé:
     *
     * Créez un BiPredicate<String, String> qui vérifie si deux chaînes ont la même longueur.
     * Testez ce BiPredicate avec différentes paires de chaînes.
     * Exercice 2: Relation Numérique
     * Objectif: Utiliser un BiPredicate pour tester une relation numérique entre deux nombres.
     *
     * Énoncé:
     *
     * Définissez un BiPredicate<Integer, Integer> qui teste si le premier nombre est plus grand que le second.
     * Appliquez ce BiPredicate à diverses paires de nombres.
     * Exercice 3: BiPredicate avec Collections
     * Objectif: Utiliser un BiPredicate pour rechercher des éléments dans deux listes.
     *
     * Énoncé:
     *
     * Créez un BiPredicate<List<Integer>, Integer> qui teste si une liste contient un certain nombre.
     * Vérifiez si une liste contient des éléments spécifiés dans une autre liste.
     * Exercice 4: BiPredicate pour Filtrage Croisé
     * Objectif: Utiliser un BiPredicate pour effectuer des filtrages croisés entre deux ensembles de données.
     *
     * Énoncé:
     *
     * Utilisez un BiPredicate<String, List<String>> pour vérifier si un mot est présent dans une liste de mots.
     * Appliquez ce BiPredicate pour filtrer une liste de mots selon leur présence dans une autre liste.
     * Exercice 5: BiPredicate dans les Streams
     * Objectif: Intégrer BiPredicate pour des comparaisons dans les opérations de streams.
     *
     * Énoncé:
     *
     * Utilisez un BiPredicate pour comparer des éléments de deux listes différentes dans un flux et produire une nouvelle liste résultante.
     */
}
