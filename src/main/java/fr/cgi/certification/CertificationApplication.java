package fr.cgi.certification;

import net.datafaker.Faker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class CertificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CertificationApplication.class, args);
		methodeCode();
	}

	static void methodeCode(){
		System.out.println("MethodeCode");
		List<UserDto> userDtos = getListUser();

		var str = "Zoo";
		StringStart methodRef = str::startsWith;
		StringStart lambda = s -> str.startsWith(s);

		System.out.println(methodRef.beginningCheck("A")); // false
		System.out.println(lambda.beginningCheck("Z")); // TRUE


	}



	static List<UserDto> getListUser(){
		List<UserDto> userDtos = new ArrayList<>();

		for (int i = 0; i < 1000; i++) {
			UserDto userDto = new UserDto();
			Faker faker = new Faker();
			userDto.setNom(faker.harryPotter().character());
			userDto.setHouse(faker.harryPotter().house());
			userDto.setAdress(faker.harryPotter().location());
			userDtos.add(userDto);
		}


		return userDtos;

	}

}
