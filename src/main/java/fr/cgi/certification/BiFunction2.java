package fr.cgi.certification;
import java.util.Map;
import java.util.function.BiFunction;


public class BiFunction2 {

    /**
     *
     * Exercice 1: Addition Simple
     * Objectif: Utiliser une BiFunction pour additionner deux nombres.
     *
     * Énoncé:
     *
     * Créez une BiFunction<Integer, Integer, Integer> qui additionne deux nombres.
     * Testez cette fonction avec différentes paires de nombres.
     * Exercice 2: Concaténation de Chaînes
     * Objectif: Utiliser une BiFunction pour concaténer deux chaînes de caractères avec un espace entre elles.
     *
     * Énoncé:
     *
     * Écrivez une BiFunction<String, String, String> qui prend deux chaînes et les concatène avec un espace.
     * Appliquez cette fonction à différentes paires de chaînes.
     * Exercice 3: Calcul de Moyenne
     * Objectif: Calculer la moyenne de deux nombres en utilisant une BiFunction.
     *
     * Énoncé:
     *
     * Définissez une BiFunction<Double, Double, Double> pour calculer la moyenne de deux nombres.
     * Testez cette fonction avec des nombres.
     * Exercice 4: Mapping Clé-Valeur
     * Objectif: Utiliser une BiFunction pour transformer un dictionnaire.
     *
     * Énoncé:
     *
     * Utilisez une BiFunction<String, Integer, String> pour transformer les valeurs d'un Map<String, Integer>, en ajoutant " ans" après chaque valeur.
     * Testez cette fonction en modifiant un dictionnaire existant.
     * Exercice 5: Comparaison de Longueur
     * Objectif: Comparer la longueur de deux chaînes de caractères.
     *
     * Énoncé:
     *
     * Créez une BiFunction<String, String, Boolean> qui retourne true si les deux chaînes ont la même longueur, sinon false.
     * Testez cette fonction avec différentes paires de chaînes.
     *
     */

    public static void main(String[] args) {
        BiFunction<Integer, Integer, Integer> add = Integer::sum;
        System.out.println(add.apply(10, 20)); // 30
    }

    public static void main2(String[] args) {
        BiFunction<String, String, String> concatenate = (a, b) -> a + " " + b;
        System.out.println(concatenate.apply("Hello", "World")); // Hello World
    }

    public static void main3(String[] args) {
        BiFunction<Double, Double, Double> average = (x, y) -> (x + y) / 2;
        System.out.println(average.apply(10.0, 20.0)); // 15.0
    }

    public static void main4(String[] args) {
        Map<String, Integer> ageMap = Map.of("Alice", 25, "Bob", 30);
        BiFunction<String, Integer, String> addYears = (name, age) -> age + " ans";

        ageMap.forEach((name, age) -> System.out.println(name + ": " + addYears.apply(name, age)));
    }

    public static void main5(String[] args) {
        BiFunction<String, String, Boolean> sameLength = (a, b) -> a.length() == b.length();
        System.out.println(sameLength.apply("test", "best")); // true
        System.out.println(sameLength.apply("hello", "world!")); // false
    }

}
