package fr.cgi.certification;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;


public class BiPredicate2 {


    /**
     * Exercice 1: Comparaison Simple
     * Objectif: Utiliser un BiPredicate pour comparer deux chaînes de caractères par leur longueur.
     *
     * Énoncé:
     *
     * Créez un BiPredicate<String, String> qui vérifie si deux chaînes ont la même longueur.
     * Testez ce BiPredicate avec différentes paires de chaînes.
     * Exercice 2: Relation Numérique
     * Objectif: Utiliser un BiPredicate pour tester une relation numérique entre deux nombres.
     *
     * Énoncé:
     *
     * Définissez un BiPredicate<Integer, Integer> qui teste si le premier nombre est plus grand que le second.
     * Appliquez ce BiPredicate à diverses paires de nombres.
     * Exercice 3: BiPredicate avec Collections
     * Objectif: Utiliser un BiPredicate pour rechercher des éléments dans deux listes.
     *
     * Énoncé:
     *
     * Créez un BiPredicate<List<Integer>, Integer> qui teste si une liste contient un certain nombre.
     * Vérifiez si une liste contient des éléments spécifiés dans une autre liste.
     * Exercice 4: BiPredicate pour Filtrage Croisé
     * Objectif: Utiliser un BiPredicate pour effectuer des filtrages croisés entre deux ensembles de données.
     *
     * Énoncé:
     *
     * Utilisez un BiPredicate<String, List<String>> pour vérifier si un mot est présent dans une liste de mots.
     * Appliquez ce BiPredicate pour filtrer une liste de mots selon leur présence dans une autre liste.
     * Exercice 5: BiPredicate dans les Streams
     * Objectif: Intégrer BiPredicate pour des comparaisons dans les opérations de streams.
     *
     * Énoncé:
     *
     * Utilisez un BiPredicate pour comparer des éléments de deux listes différentes dans un flux et produire une nouvelle liste résultante.
     */

    public static void main(String[] args) {
        BiPredicate<String, String> sameLength = (s1, s2) -> s1.length() == s2.length();
        System.out.println(sameLength.test("hello", "world"));  // true
        System.out.println(sameLength.test("hello", "hello!")); // false
    }


    public static void main2(String[] args) {
        BiPredicate<Integer, Integer> isGreaterThan = (a, b) -> a > b;
        System.out.println(isGreaterThan.test(5, 3));  // true
        System.out.println(isGreaterThan.test(2, 3));  // false
    }

    public static void main3(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3, 4, 5);
        BiPredicate<List<Integer>, Integer> contains = List::contains;
        System.out.println(contains.test(numbers, 3)); // true
        System.out.println(contains.test(numbers, 6)); // false
    }

    public static void main4(String[] args) {
        List<String> list1 = List.of("apple", "banana", "cherry");
        List<String> list2 = List.of("banana", "date", "fig", "grape");

        BiPredicate<String, List<String>> isIn = (item, list) -> list.contains(item);
        List<String> filtered = list1.stream().filter(item -> isIn.test(item, list2)).collect(Collectors.toList());
        System.out.println(filtered); // ["banana"]
    }

    public static void main5(String[] args) {
        List<Integer> list1 = List.of(1, 2, 3, 4, 5);
        List<Integer> list2 = List.of(3, 4, 5, 6, 7);
        BiPredicate<Integer, Integer> isCommon = (a, b) -> a.equals(b);

        List<Integer> commonElements = list1.stream()
                .filter(a -> list2.stream().anyMatch(b -> isCommon.test(a, b)))
                .collect(Collectors.toList());

        System.out.println(commonElements); // [3, 4, 5]
    }


}
