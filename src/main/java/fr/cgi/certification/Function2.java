package fr.cgi.certification;
import java.util.function.Function;
import java.util.List;
import java.util.stream.Collectors;

public class Function2 {

    /**
     *
     * Exercice 1: Transformation de Chaînes
     * Objectif: Utiliser une Function pour convertir des chaînes de caractères en leur longueur.
     *
     * Énoncé:
     *
     * Créez une Function<String, Integer> qui prend une chaîne de caractères et retourne sa longueur.
     * Testez cette fonction avec plusieurs chaînes.
     * Exercice 2: Calcul Mathématique
     * Objectif: Utiliser une Function pour effectuer un calcul simple.
     *
     * Énoncé:
     *
     * Définissez une Function<Integer, Integer> qui calcule le carré d'un nombre.
     * Appliquez cette fonction à une liste de nombres et affichez les résultats.
     * Exercice 3: Manipulation de Listes
     * Objectif: Transformer des éléments d'une liste en utilisant une Function.
     *
     * Énoncé:
     *
     * Utilisez une Function<List<Integer>, List<Integer>> pour doubler chaque élément d'une liste.
     * Testez cette fonction avec une liste d'entiers.
     * Exercice 4: Composition de Functions
     * Objectif: Combiner plusieurs Function pour créer des transformations en chaîne.
     *
     * Énoncé:
     *
     * Créez une Function<String, String> pour supprimer les espaces d'une chaîne de caractères et une autre pour convertir en majuscules.
     * Composez ces fonctions et appliquez-les à une chaîne de caractères.
     * Exercice 5: Conversion de Types
     * Objectif: Utiliser une Function pour convertir d'un type à un autre.
     *
     * Énoncé:
     *
     * Écrivez une Function<String, Boolean> qui convertit une chaîne de caractères en un booléen (par exemple, "true" en true, autre chose en false).
     * Testez cette fonction avec différentes chaînes.
     */

    public static void main(String[] args) {
        Function<String, Integer> lengthFunction = String::length;
        System.out.println(lengthFunction.apply("Hello World")); // 11
    }

    public static void main2(String[] args) {
        Function<Integer, Integer> squareFunction = x -> x * x;
        List<Integer> numbers = List.of(1, 2, 3, 4, 5);
        List<Integer> squares = numbers.stream().map(squareFunction).collect(Collectors.toList());
        System.out.println(squares); // [1, 4, 9, 16, 25]
    }
    public static void main3(String[] args) {
        Function<List<Integer>, List<Integer>> doubleElements = list -> list.stream().map(x -> x * 2).collect(Collectors.toList());
        List<Integer> result = doubleElements.apply(List.of(1, 2, 3));
        System.out.println(result); // [2, 4, 6]
    }
    public static void main4(String[] args) {
        Function<String, String> removeSpaces = s -> s.replace(" ", "");
        Function<String, String> toUpperCase = String::toUpperCase;

        Function<String, String> combinedFunction = removeSpaces.andThen(toUpperCase);
        System.out.println(combinedFunction.apply("Hello World")); // HELLOWORLD
    }
    public static void main5(String[] args) {
        Function<String, Boolean> stringToBoolean = "true"::equalsIgnoreCase;
        System.out.println(stringToBoolean.apply("true"));  // true
        System.out.println(stringToBoolean.apply("false")); // false
    }



}
