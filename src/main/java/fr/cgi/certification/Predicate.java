package fr.cgi.certification;

public class Predicate {

    /**
     *
     * Exercice 1: Simple Validation
     * Objectif: Utiliser un Predicate pour valider si un nombre est positif.
     *
     * Énoncé:
     *
     * Créez un Predicate<Integer> qui vérifie si un nombre est positif.
     * Testez ce Predicate avec divers entiers.
     * Exercice 2: Filtrage de Collection
     * Objectif: Utiliser un Predicate pour filtrer des éléments d'une liste.
     *
     * Énoncé:
     *
     * Écrivez un Predicate<String> qui teste si une chaîne de caractères commence par une lettre spécifique.
     * Utilisez ce Predicate pour filtrer une liste de mots.
     * Exercice 3: Combinaison de Predicates
     * Objectif: Apprendre à combiner plusieurs Predicate en utilisant and, or, et negate.
     *
     * Énoncé:
     *
     * Définissez trois Predicate<String> : un pour vérifier si une chaîne est non nulle, un pour vérifier si elle n'est pas vide, et un pour vérifier si elle a plus de 5 caractères.
     * Combine-les pour créer un Predicate complexe.
     * Exercice 4: Predicate avec Streams
     * Objectif: Intégrer Predicate dans les opérations de stream pour filtrer des éléments.
     *
     * Énoncé:
     *
     * Utilisez un Predicate<Integer> pour filtrer tous les nombres pairs d'une liste.
     * Affichez les résultats après avoir appliqué le filtre sur un flux d'entiers.
     * Exercice 5: Suppression conditionnelle dans une Collection
     * Objectif: Utiliser un Predicate pour effectuer des suppressions conditionnelles dans une collection.
     *
     * Énoncé:
     *
     * Créez un Predicate<String> qui teste si une chaîne de caractères contient un certain mot.
     * Utilisez ce Predicate pour supprimer des éléments d'une ArrayList de chaînes.
     *
     */



}
