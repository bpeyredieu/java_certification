package fr.cgi.certification;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.function.Predicate;


public class Predicate2 {

    /**
     *
     * Exercice 1: Simple Validation
     * Objectif: Utiliser un Predicate pour valider si un nombre est positif.
     *
     * Énoncé:
     *
     * Créez un Predicate<Integer> qui vérifie si un nombre est positif.
     * Testez ce Predicate avec divers entiers.
     * Exercice 2: Filtrage de Collection
     * Objectif: Utiliser un Predicate pour filtrer des éléments d'une liste.
     *
     * Énoncé:
     *
     * Écrivez un Predicate<String> qui teste si une chaîne de caractères commence par une lettre spécifique.
     * Utilisez ce Predicate pour filtrer une liste de mots.
     * Exercice 3: Combinaison de Predicates
     * Objectif: Apprendre à combiner plusieurs Predicate en utilisant and, or, et negate.
     *
     * Énoncé:
     *
     * Définissez trois Predicate<String> : un pour vérifier si une chaîne est non nulle, un pour vérifier si elle n'est pas vide, et un pour vérifier si elle a plus de 5 caractères.
     * Combine-les pour créer un Predicate complexe.
     * Exercice 4: Predicate avec Streams
     * Objectif: Intégrer Predicate dans les opérations de stream pour filtrer des éléments.
     *
     * Énoncé:
     *
     * Utilisez un Predicate<Integer> pour filtrer tous les nombres pairs d'une liste.
     * Affichez les résultats après avoir appliqué le filtre sur un flux d'entiers.
     * Exercice 5: Suppression conditionnelle dans une Collection
     * Objectif: Utiliser un Predicate pour effectuer des suppressions conditionnelles dans une collection.
     *
     * Énoncé:
     *
     * Créez un Predicate<String> qui teste si une chaîne de caractères contient un certain mot.
     * Utilisez ce Predicate pour supprimer des éléments d'une ArrayList de chaînes.
     *
     */

    public static void main(String[] args) {
        Predicate<Integer> isPositive = x -> x > 0;
        System.out.println(isPositive.test(10));  // true
        System.out.println(isPositive.test(-10)); // false
    }

    public static void main2(String[] args) {
        List<String> words = List.of("apple", "banana", "avocado", "grape", "apricot");
        Predicate<String> startsWithA = word -> word.startsWith("a");
        List<String> filtered = words.stream().filter(startsWithA).collect(Collectors.toList());
        System.out.println(filtered);
    }


    public static void main3(String[] args) {
        Predicate<String> isNotNull = s -> s != null;
        Predicate<String> isNotEmpty = s -> !s.isEmpty();
        Predicate<String> isLongerThan5 = s -> s.length() > 5;

        Predicate<String> combined = isNotNull.and(isNotEmpty).and(isLongerThan5);
        System.out.println(combined.test("Hello World")); // true
        System.out.println(combined.test(""));            // false
    }

    public static void main4(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        Predicate<Integer> isEven = n -> n % 2 == 0;
        List<Integer> evens = numbers.stream().filter(isEven).collect(Collectors.toList());
        System.out.println(evens);
    }

    public static void main5(String[] args) {
        List<String> phrases = new ArrayList<>();
        phrases.add("hello world");
        phrases.add("world peace");
        phrases.add("hello peace");
        phrases.add("good morning");

        Predicate<String> containsWorld = s -> s.contains("world");
        phrases.removeIf(containsWorld);
        System.out.println(phrases);
    }

}
