package fr.cgi.certification;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;


public class Consumer2 {

    /**
     *Exercice 1: Consumer Basique
     * Objectif: Apprendre à créer et utiliser un Consumer simple pour effectuer une action sur un objet.
     *
     * Énoncé:
     *
     * Créez un Consumer qui prend une chaîne de caractères et l'imprime en majuscules.
     * Testez ce Consumer avec différentes chaînes.
     * Exercice 2: Consumer avec Collections
     * Objectif: Utiliser un Consumer pour effectuer des opérations sur des éléments d'une collection.
     *
     * Énoncé:
     *
     * Écrivez un Consumer qui modifie une liste de nombres en multipliant chaque élément par 10.
     * Appliquez ce Consumer à une liste de nombres et affichez le résultat.
     *
     * Exercice 3: Chaining Consumers
     * Objectif: Pratiquer le chaînage de Consumer.
     *
     * Énoncé:
     *
     * Créez deux Consumer: un qui ajoute un préfixe à une chaîne de caractères et un autre qui ajoute un suffixe.
     * Chaînez ces deux Consumer et appliquez-les à une chaîne de caractères pour observer l'effet cumulatif.
     * Exercice 4: Consumer avec forEach
     * Objectif: Intégrer Consumer dans des méthodes de parcours, comme forEach.
     *
     * Énoncé:
     *
     * Utilisez un Consumer pour imprimer tous les éléments d'un Set de chaînes de caractères.
     * Ajoutez une logique supplémentaire pour imprimer "Vide" si l'ensemble est vide avant d'utiliser le forEach.
     * Exercice 5: Consumer dans les Streams
     * Objectif: Utiliser Consumer dans le contexte des Streams pour des opérations intermédiaires ou finales.
     *
     * Énoncé:
     *
     * Créez un flux d'entiers.
     * Utilisez un Consumer pour imprimer chaque nombre après l'avoir augmenté de 5.
     * Utilisez le flux pour appliquer ce Consumer.
     */

    public static void main1(String[] args) {
        Consumer<String> printUpperCase = s -> System.out.println(s.toUpperCase());
        printUpperCase.accept("hello world");
    }

    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        Consumer<List<Integer>> multiplyByTen = list -> {
            for (int i = 0; i < list.size(); i++) {
                list.set(i, list.get(i) * 10);
            }
        };


        multiplyByTen.accept(numbers);
        System.out.println(numbers);
    }









    public static void main3(String[] args) {
        Consumer<String> addPrefix = s -> System.out.print("Prefix_" + s);
        Consumer<String> addSuffix = s -> System.out.println(s + "_Suffix");

        Consumer<String> combinedConsumer = addPrefix.andThen(addSuffix);
        combinedConsumer.accept("Example");
    }

    public static void main4(String[] args) {
        Set<String> strings = new HashSet<>();
        strings.add("Java");
        strings.add("Consumer");
        strings.add("Example");

        Consumer<Set<String>> printSet = set -> {
            if (set.isEmpty()) {
                System.out.println("Vide");
            } else {
                set.forEach(System.out::println);
            }
        };

        printSet.accept(strings);
    }

    public static void main5(String[] args) {
        Consumer<Integer> increaseAndPrint = number -> System.out.println(number + 5);
        Stream.of(1, 2, 3, 4, 5).forEach(increaseAndPrint);
    }

}
